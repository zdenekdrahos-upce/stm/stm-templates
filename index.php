<?php
require_once(__DIR__ . '/../_sports_table_manager/stm/bootstrap.php');
define('STM_TEMPLATE_ROOT', __DIR__ . '/templates/');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>STM Templates</title>
        <link href="style.css" type="text/css" rel="stylesheet" />
    </head>
    <body id="stm">

<?php 
# CLUBS
echo '<h2>Club/Club</h2>';
$club = \STM\StmFactory::find()->Club->findById(1);
include(STM_TEMPLATE_ROOT . 'club/club.php');
// club teams
$teams = \STM\StmFactory::find()->Team->findByClub($club);
include(STM_TEMPLATE_ROOT . 'club/teams.php');
// club members
$clubMembers = \STM\StmFactory::find()->ClubMember->findByClub($club);
include(STM_TEMPLATE_ROOT . 'club/members.php');
echo '<hr />';

# TEAM
echo '<h2>Team/Tým</h2>';
$team = \STM\StmFactory::find()->Team->findById(1);
$teamDetails = $team->toArray();
include(STM_TEMPLATE_ROOT . 'team/team.php');
// last match
$match = \STM\StmFactory::find()->Match->getLastPlayedMatch(null, $team->getId());
include(STM_TEMPLATE_ROOT . 'team/last-match.php');
// next match
$match = \STM\StmFactory::find()->Match->getFirstUpcomingMatch(null, $team->getId());
include(STM_TEMPLATE_ROOT . 'team/next-match.php');
// team in competitions
$competitions = \STM\StmFactory::find()->Competition->findByTeam($team);
include(STM_TEMPLATE_ROOT . 'team/competitions.php');
// team members
$teamMembers = \STM\StmFactory::find()->TeamMember->findByTeam($team);
include(STM_TEMPLATE_ROOT . 'team/members.php');
echo '<hr />';

# COMPETITION
echo '<h2>Competition/Soutěž</h2>';
$competition = \STM\StmFactory::find()->Competition->findById(12);
include(STM_TEMPLATE_ROOT . 'competition/competition.php');
// teams from competition
$teams = $competition->getTeams();
include(STM_TEMPLATE_ROOT . 'competition/teams.php');
// matches
$matches = \STM\StmCache::getAllCompetitionMatches($competition);
include(STM_TEMPLATE_ROOT . 'competition/matches.php');
// other competitions from category
$category = $competition->getIdCategory();
$competitions = \STM\StmFactory::find()->Competition->findByCategory($category);
include(STM_TEMPLATE_ROOT . 'competition/other-competitions.php');
echo '<hr />';

# SEASON
echo '<h2>Season/Sezóna</h2>';
$competition = \STM\StmFactory::find()->Competition->findById(2);
include(STM_TEMPLATE_ROOT . 'competition/competition.php');

// teams from competition
$teams = $competition->getTeams();
include(STM_TEMPLATE_ROOT . 'competition/teams.php');

// matches
$matches = \STM\StmCache::getAllCompetitionMatches($competition);
include(STM_TEMPLATE_ROOT . 'competition/matches.php');

// season tables
$seasonTable = \STM\StmCache::getSeasonTable($competition);
$yourTeams = $competition->getIdOfYourTeams();
$table = $seasonTable->getTableFull();
include(STM_TEMPLATE_ROOT . 'competition/table-big.php');
$table = $seasonTable->getTableHome();
include(STM_TEMPLATE_ROOT . 'competition/table-small.php');
$table = $seasonTable->getTableAway();
include(STM_TEMPLATE_ROOT . 'competition/table-small.php');
// cross table
$crossTable = \STM\Match\Table\Cross\CrossTableFactory::getSeasonCrossTable($competition);
include(STM_TEMPLATE_ROOT . 'competition/season-cross-table.php');
// season positions of teams
$seasonTeamsPositions = \STM\StmCache::getTeamsPositionsInTable($competition);
include(STM_TEMPLATE_ROOT . 'competition/season-positions.php');
// other competitions from category
$category = $competition->getIdCategory();
$competitions = \STM\StmFactory::find()->Competition->findByCategory($category);
include(STM_TEMPLATE_ROOT . 'competition/other-competitions.php');
echo '<hr />';

# PLAYOFF
echo '<h2>Plaoyff</h2>';
$competition = \STM\StmFactory::find()->Competition->findById(10);
include(STM_TEMPLATE_ROOT . 'competition/competition.php');
// teams from competition
$teams = $competition->getTeams();
include(STM_TEMPLATE_ROOT . 'competition/teams.php');
// playoff series
// $series = \STM\StmFactory::find()->PlayoffSerie->findByPlayoffRound($competition, 1);
$series = \STM\StmFactory::find()->PlayoffSerie->findByPlayoff($competition);
include(STM_TEMPLATE_ROOT . 'competition/playoff-series.php');
// matches from playoff serie
$serie = \STM\StmFactory::find()->PlayoffSerie->findById(1);
$matchSelection = array(
    'matchType' => \STM\Match\MatchSelection::ALL_MATCHES,
    'loadScores' => true,
    'loadPeriods' => false,
    'playoffSerie' => $serie,
);  
$matches = \STM\StmFactory::find()->Match->find($matchSelection);
include(STM_TEMPLATE_ROOT . 'competition/playoff-serie-matches.php');
// playoff tree
$treeBuilder = new \STM\Competition\Playoff\Tree\PlayoffTreeBuilder($competition);
$treeAdapter = new \STM\Competition\Playoff\Tree\PlayoffTreeAdapter($treeBuilder);
$playoffRounds = $treeAdapter->getRounds();
include(STM_TEMPLATE_ROOT . 'competition/playoff-tree.php');
// other competitions from category
$category = $competition->getIdCategory();
$competitions = \STM\StmFactory::find()->Competition->findByCategory($category);
include(STM_TEMPLATE_ROOT . 'competition/other-competitions.php');
echo '<hr />';

# MATCH
echo '<h2>Match/Zápas</h2>';
$myMatch = \STM\StmFactory::find()->Match->findById(6);
$match = $myMatch;
include(STM_TEMPLATE_ROOT . 'match/match.php');
// match periods
$matchPeriods = $myMatch->getPeriods();
include(STM_TEMPLATE_ROOT . 'match/periods.php');
// match detail
$matchDetail = \STM\StmFactory::find()->MatchDetail->findById($myMatch->getId());
include(STM_TEMPLATE_ROOT . 'match/detail.php');
// match referees
$matchReferees = \STM\StmFactory::find()->MatchReferee->findByMatch($myMatch);
include(STM_TEMPLATE_ROOT . 'match/referees.php');
// team actions
$matchTeamActions = \STM\StmFactory::find()->MatchTeamAction->findByMatch($myMatch);
include(STM_TEMPLATE_ROOT . 'match/actions-teams.php');
// player actions
$matchPlayerActions = \STM\StmFactory::find()->MatchPlayerAction->findByMatch($myMatch);
include(STM_TEMPLATE_ROOT . 'match/actions-players.php');
// lineups
$lineups = new \STM\Match\Lineup\Lineups($myMatch);
include(STM_TEMPLATE_ROOT . 'match/lineups.php');
// match predictions statistics
$matchPredictionsStatistics = \STM\StmFactory::find()->PredictionStats->findByMatch($myMatch);
$statistics = $matchPredictionsStatistics->toArray();
include(STM_TEMPLATE_ROOT . 'match/predictions-statistics.php');
echo '<hr />';

# PERSON
echo '<h2>Person/Osoba</h2>';
$myPerson = \STM\StmFactory::find()->Person->findById(1);
$person = $myPerson;
$personDetails = $person->toArray();
$birthDate = \STM\Utils\Dates::convertDatetimeToString($personDetails['birth_date'], '', 'd.m.Y');
include(STM_TEMPLATE_ROOT . 'person/person.php');
// person attribute
$personAttributes = \STM\StmFactory::find()->PersonAttribute->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/attributes.php');
// member of team
$memberOfTeams = \STM\StmFactory::find()->TeamMember->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/teams.php');
// member of club
$memberOfClubs = \STM\StmFactory::find()->ClubMember->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/clubs.php');
// referee in match
$refereeInMatches = \STM\StmFactory::find()->MatchReferee->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/referees.php');
// matches with statistic
$matchPlayerActions = \STM\StmFactory::find()->MatchPlayerAction->findByPerson($myPerson);
include(STM_TEMPLATE_ROOT . 'person/matches.php');
echo '<hr />';

# MATCH SELECTION
echo '<h2>Matches/Zápasy</h2>';
$matchSelection = array(
    'matchType' => \STM\Match\MatchSelection::PLAYED_MATCHES,
    'loadScores' => true,
    'loadPeriods' => true,
    'dates' => array('min' => 'now - 3 years', 'max' => 'now + 1 year'),
);
// matches
$matches = \STM\StmFactory::find()->Match->find($matchSelection);
include(STM_TEMPLATE_ROOT . 'matches.php');
// result tables (like in season, but LIMIT is NOT applied)
$resultTable = \STM\Match\Table\ResultTable::getTable($matchSelection, new \STM\Match\Table\TablePoints(), STM_MATCH_PERIODS);
$yourTeams = array(1);
$table = $resultTable->getTableFull();
include(STM_TEMPLATE_ROOT . 'competition/table-small.php');
// predictions statistics
$matchPredictionsStatistics = \STM\StmFactory::find()->PredictionStats->find($matchSelection);
$statistics = $matchPredictionsStatistics->toArray();
include(STM_TEMPLATE_ROOT . 'match/predictions-statistics.php');
// prediction chart
$calculator = new \STM\Prediction\Chart\PredictionCalculator();
$predictionsChart = \STM\StmFactory::find()->PredictionChart->find($matchSelection, $calculator);
include(STM_TEMPLATE_ROOT . 'match/predictions-chart.php');
// statistics of team in selected matches - second parameter = ID TEAM
$statistics = \STM\Match\Stats\TeamMatchesStatsFactory::loadFromDatabase($matchSelection, 1);
include(STM_TEMPLATE_ROOT . 'statistics/team.php');
?>

    </body>
</html>
