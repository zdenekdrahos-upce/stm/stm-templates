<h3>Team's members/Členové týmu</h3>
<?php if (is_array($teamMembers)): ?>
    <table>
        <tr>
            <th>Tým</th>
            <th>Osoba</th>
            <th>Pozice</th>
            <th>Je hráč?</th>
        </tr>    
        <tr>
            <th>Team</th>
            <th>Person</th>
            <th>Position</th>
            <th>Is it player?</th>
        </tr>        
        <?php foreach ($teamMembers as $member): extract($member->toArray()); ?>
        <tr>
            <td><?php echo $team; ?></td>
            <td><strong><?php echo $person; ?></strong></td>
            <td><?php echo $position; ?></td>
            <td><?php echo $member->isTeamPlayer() ? 'Yes/Ano' : 'No/Ne'; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>Nebyli načteni žádní členové týmu</p>
    <p>No members</p>
<?php endif; ?>