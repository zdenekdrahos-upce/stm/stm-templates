
<h3>Player's actions/Hráčské akce</h3>
<?php if (empty($matchPlayerActions)): ?>
    <p>No actions/Žádné akce</p>
<?php else: ?>
    <table>
        <thead>
            <tr>
                <th>Team/Tým</th>
                <th>Player/Hráč</th>
                <th>Action/Akce</th>
                <th>When Action Happened/Kdy se akce odehrála</th>
                <th>Komentáře</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($matchPlayerActions as $matchPlayerAction): extract($matchPlayerAction->toArray()); ?>
            <tr>
                <td><?php echo $name_team; ?></td>
                <td><?php echo $name_player; ?></td>
                <td><?php echo $name_action; ?></td>
                <td><?php echo $minute_action; ?></td>
                <td><?php echo $action_comment; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
