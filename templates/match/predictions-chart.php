
<h3>Predictions chart/Žebříček tipérů</h3>
<table>
    <tr>        
        <th>Username/Jméno</th>
        <th>Predictions count/Počet tipů</th>
        <th>Cheat/Podvody</th>        
        <th>Exact predictions/Přesné tipy</th>
        <th>Predictions succes/Úspěšnost</th>
        <th>Points/Body</th>
    </tr>
    <?php foreach ($predictionsChart->getChart() as $row): ?>
    <tr>
        <td><strong><?php echo $row->username; ?></strong></td>
        <td><?php echo $row->predictions; ?></td>        
        <td><?php echo $row->cheatPredictions; ?></td>        
        <td><?php echo $row->exactPredictions; ?></td>
        <td><?php echo \STM\Stats\Stats::getPercentage($row->successPredictions, $row->predictions); ?>%</td>
        <td><?php echo $row->points; ?></td>
    </tr>
    <?php endforeach; ?>
</table>
