<?php if($match instanceof \STM\Match\Match): extract($match->toArray()); ?>
    <table>
        <tr>
            <td><strong><?php echo $team_home; ?></strong></td>
            <td><?php echo $match->hasScore() ? "{$score_home}:{$score_away}" : '-:-'; ?></td> 
            <td><strong><?php echo $team_away; ?></strong></td>
        </tr>
        <tr>
            <td colspan="3"><em><?php echo \STM\Utils\Dates::convertDatetimeToString($date, 'Not Set', 'd.m.Y H:i'); ?></em></td>
        </tr>
    </table>
<?php else: ?>
    <p>Zápas se nepodařilo načíst nebo neexistuje</p>
    <p>Match wasn't loaded or not existing match</p>
<?php endif; ?>