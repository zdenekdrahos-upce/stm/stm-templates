<?php if (empty($matchLineup)): ?>
    <p>No players/Žádní hráči</p>
<?php else: ?>
    <table>
        <thead>
            <tr>        
                <th>Player/Hráč</th>
                <th>Position/pozice</th>
                <th>Since/Od</th>
                <th>To/Do</th>
                <th>Střídající hráč</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($matchLineup as $matchPlayer): extract($matchPlayer->toArray()); ?>
            <tr>
                <td><string><?php echo $player; ?></strong></td>
                <td><?php echo $position; ?></td>        
                <td><?php echo $minute_in != '' ? $minute_in : '-'; ?></td>
                <td><?php echo $minute_out != '' ? $minute_out : '-'; ?></td>
                <td><?php 
                $substitution = $lineups->getSubstitutionPlayer($matchPlayer->getIdSubstitutionPlayer());
                if ($substitution) {
                    $temp = $substitution->toArray();
                    echo $temp['player'];
                } else {
                    echo '-';
                }
                ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>