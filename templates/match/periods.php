<h3>Periods/Periody</h3>
<?php if(!empty($matchPeriods)): ?>
    <table>
        <tr>
            <th>Number/Číslo</th>
            <th>Home/Domácí</th>
            <th>Away/Hosté</th>
            <th>Note/Poznámka</th>
        </tr>
        <?php foreach ($matchPeriods as $period): extract($period->toArray()); ?>
        <tr>
            <td><?php echo $id_period; ?></td>
            <td><strong><?php echo $score_home; ?></strong></td>
            <td><strong><?php echo $score_away; ?></strong></td>
            <td><?php echo $note; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>Zápasové periody se nepodařilo načíst nebo neexistují</p>
    <p>Match periods weren't loaded or not existing match periods</p>
<?php endif; ?>