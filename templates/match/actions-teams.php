
<h3>Team's actions/Týmové akce</h3>
<?php if (empty($matchTeamActions)): ?>
    <p>No actions/Žádné akce</p>
<?php else: ?>
    <table>
        <tr>        
            <th>Action/Akce</th>
            <th>Home/Domácí</th>
            <th>Away/Hosté</th>
        </tr>
        <?php foreach($matchTeamActions as $teamAction): extract($teamAction->toArray()); ?>
        <tr>
            <td><?php echo $name_action; ?></td>
            <td><?php echo $value_home; ?></td>
            <td><?php echo $value_away; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>
