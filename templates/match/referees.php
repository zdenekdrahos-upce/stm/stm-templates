<h3>Referees/rozhodčí</h3>
<?php if (empty($matchReferees)): ?>
    <p>No referees/Žádní rozhodčí</p>
<?php else: ?>
    <table>
        <tr>        
            <th>Referee/Rozhodčí</th>
            <th>Position/Pozice</th>
        </tr>
        <?php foreach ($matchReferees as $referee): $referee = $referee->toArray(); ?>
        <tr>
            <td><?php echo $referee['person']; ?></td>
            <td><?php echo $referee['position']; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>