<h3>Detail</h3>
<?php if ($matchDetail instanceof \STM\Match\Detail\MatchDetail): extract($matchDetail->toArray()); ?>
    <ul>
        <li>Spectators/Diváci: <strong><?php echo $spectators; ?></strong></li>
        <li>Stadium/Stadion: <strong><?php echo $stadium; ?></strong></li>
        <li>Comment/Komentář: <strong><?php echo $match_comment; ?></strong></li>
    </ul>
<?php else: ?>
    <p>No detail/Bez detailu</p>
<?php endif; ?>