
<h3>Predictions stats/Statistiky tipování</h3>
<ul>
    <li>Predictions count/Počet tipů: <strong><?php echo $statistics['predictions']; ?></strong></li>
    <li>Success/Úspěšnost: <strong><?php echo $statistics['success_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['success_predictions'], $statistics['predictions']); ?>%)</strong></li>
    <li>Exact predictions/Přesné tipy: <strong><?php echo $statistics['exact_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['exact_predictions'], $statistics['predictions']); ?>%)</strong></li>
</ul>

<h4>How many times.../Kolikrát byla tipována...</h4>
<ul>
    <li>Home win/Výhra domácích: <strong><?php echo $statistics['win_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['win_predictions'], $statistics['predictions']); ?>%)</strong></li>
    <li>Draw/remíza: <strong><?php echo $statistics['draw_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['draw_predictions'], $statistics['predictions']); ?>%)</strong></li>
    <li>Home loss/Prohra domácích: <strong><?php echo $statistics['loss_predictions']; ?> (<?php echo \STM\Stats\Stats::getPercentage($statistics['loss_predictions'], $statistics['predictions']); ?>%)</strong></li>
</ul>