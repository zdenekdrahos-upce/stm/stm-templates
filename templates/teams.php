
<?php if (empty($teams)): ?>
    <p>No teams/Žádné týmy</p>
<?php else: ?>
    <ul>
        <?php foreach($teams as $team): ?>
        <li><?php echo $team; ?></li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>
