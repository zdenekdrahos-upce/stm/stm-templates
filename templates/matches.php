
<?php if(is_array($matches)): ?>
        <table>
        <?php foreach ($matches as $match): extract($match->toArray());  ?>    
            <tr>
                <td rowspan="2"><strong><?php echo $team_home; ?></strong></td>
                <td><?php echo $match->hasScore() ? ($score_home . ' : ' . $score_away) : '-:-'; ?></td>    
                <td rowspan="2"><strong><?php echo $team_away; ?></strong></td>
            </tr>
            <tr>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date, 'Not Set', 'd.m.Y H:i') ?></td>  
            </tr>
        <?php endforeach; ?>
        </table>
<?php else: ?>
    <p>Zápasy se nepodařilo načíst nebo žádný neexistuje</p>
    <p>Matches weren't loaded or no matches</p>
<?php endif; ?>