<?php 
$yourTeams = isset($yourTeams) ? $yourTeams : array();
?>
<h3>Small table/Malá tabulka</h3>
<?php if(is_array($table)): ?>
    <table>
        <?php foreach ($table as $pos => $row): ?>
        <tr<?php echo in_array($row->idTeam, $yourTeams) ? ' class="team"' : ''; ?>>
            <td><?php echo $pos + 1; ?>.</td>
            <td><?php echo $row->name; ?></td>
            <td><strong><?php echo $row->points; ?></strong></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>Tabulku se nepodařilo načíst</p>
    <p>Table wasn't loaded</p>
<?php endif; ?>