<h3>Teams Positions/Pozice týmů</h3>
<?php foreach($seasonTeamsPositions->getPositionOfAllTeams() as $idTeam => $info): ?>
<h4><?php echo $info['team']->__toString(); ?></h4>
<table>
    <tr>
        <th>Season Round/Kolo Sezony</th>
        <th>Position/Pozice</th>
    </tr>
    <?php foreach($info['positions'] as $round => $position): ?>
    <tr>
        <td><?php echo $round; ?></td>
        <td><strong><?php echo $position ? "{$position}." : '-'; ?></strong></td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endforeach; ?>