<?php
$yourTeams = isset($yourTeams) ? $yourTeams : array();
?>

<h3>Complete table/Kompletní tabulka</h3>
<?php if (is_array($table)): ?>
    <table>
        <thead>
            <tr>
                <th>Pozice</th>
                <th>Tým</th>
                <th>Zápasy</th>
                <th>Výhry</th>
                <th>Remízy</th>
                <th>Prohry</th>
                <th>Skóre</th>        
                <th>Body</th>
            </tr>
            <tr>
                <th>Position</th>
                <th>Team</th>
                <th>Matches</th>
                <th>Win</th>
                <th>Draw</th>
                <th>Loss</th>
                <th>Score</th>
                <th>Points</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($table as $pos => $row): ?>
                <tr<?php echo in_array($row->idTeam, $yourTeams) ? ' class="team"' : ''; ?>>
                    <td><?php echo $pos + 1; ?>.</td>      
                    <td><strong><?php echo $row->name; ?></strong></td>
                    <td><?php echo $row->matches; ?></td>
                    <td><?php echo $row->win + $row->winInOt; ?></td>
                    <td><?php echo $row->draw; ?></td>
                    <td><?php echo $row->loss + $row->lossInOt; ?></td>
                    <td><?php echo $row->goalFor . ':' . $row->goalAgainst; ?></td>
                    <td><strong><?php echo $row->points; ?></strong></td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <p>Tabulku se nepodařilo načíst</p>
    <p>Table wasn't loaded</p>
<?php endif; ?>
