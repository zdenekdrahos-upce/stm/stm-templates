<?php
extract($competition->toArray());
?>

<h3>Competition info/Informace o soutěži</h3>
<ul>
    <li>Competition/Soutěž: <strong><?php echo $name; ?></strong></li>
    <li>Type/Typ: <strong><?php echo $type; ?></strong></li>
    <li>Match Periods/Počet period zápasu: <strong><?php echo $match_periods; ?></strong></li>
    <li>Since/Od: <strong><?php echo \STM\Utils\Dates::convertDatetimeToString($date_start, '-', 'd.m.Y'); ?></strong></li>
    <li>To/Do: <strong><?php echo \STM\Utils\Dates::convertDatetimeToString($date_end, '-', 'd.m.Y'); ?></strong></li>
    <li>Category/Kategorie: <strong><?php echo $category; ?></strong></li>
    <li>Score calculation/Výpočet skóre: <strong><?php echo $score_type; ?></strong></li>
    <li>Status: <strong><?php echo $status; ?></strong></li>
</ul>    