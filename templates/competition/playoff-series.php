<h3>Playoff series/Série z playoff</h3>
<table>
    <tr>
        <th>Round/Kolo</th>
        <th>Home/Domácí</th>
        <th>Away/Hosté</th>
        <th>Score (win-draw-loss)/Skóre(výhra-remíza-prohra)</th>
        <th>Finished/Dohráno</th>
    </tr>
    <?php foreach ($series as $serie): extract($serie->toArray()) ?>
    <tr>
        <td><?php echo $round; ?></td>
        <td><?php echo $team_1; ?></td>
        <td><?php echo $team_2; ?></td>
        <td><?php echo "{$wins_team_1}-{$draws}-{$wins_team_2}"; ?></td>
        <td><?php echo $serie->isFinished($competition) ? 'Yes/Ano' : 'No/Ne'; ?></td>
    </tr>
    <?php endforeach; ?>
</table>