
<h3>Playoff tree/Playoff pavouk</h3>
<?php foreach ($playoffRounds as $round): ?>
<table class="playoff" style="height: <?php echo $competition->getCountTeams() * 30; ?>px;float:left;width:250px" >
    <?php foreach ($round as $serie): ?>
    <tr>
        <td>
            <table>
                <?php if ($serie instanceof \STM\Competition\Playoff\Serie\PlayoffSerie): extract($serie->toArray()); ?>
                <tr>
                    <td class="seeded"><?php echo $competition->getSeededOfTeam($id_team_1); ?></td>
                    <td<?php echo $serie->isFinished($competition) && $wins_team_1 > $wins_team_2 ? ' class="winner" ' : ''; ?>><?php echo $team_1; ?></td>
                    <td rowspan="2" class="score"><?php echo $wins_team_1 . ':' . $wins_team_2; ?></td>
                </tr>
                <tr>
                    <td class="seeded"><?php echo $competition->getSeededOfTeam($id_team_2); ?></td>
                    <td<?php echo $serie->isFinished($competition) && $wins_team_1 < $wins_team_2 ? ' class="winner" ' : ''; ?>><?php echo $team_2; ?></td>
                </tr>
                <?php else: ?>
                <tr>
                    <td><?php echo $serie; ?></td>
                </tr>
                <?php endif; ?>
            </table>
        </td>
    </tr>
    <?php endforeach; ?>
</table>
<?php endforeach; ?>