
<?php if (is_array($competitions)): ?>
    <table>
        <tr>
            <th>Soutěž</th>
            <th>Typ</th>
            <th>Počet period zápasu</th>
            <th>Od</th>
            <th>Do</th>
            <th>Kategorie</th>
            <th>Výpočet skóre</th>
            <th>Status</th>
        </tr>    
        <tr>
            <th>Competition</th>
            <th>Type</th>
            <th>Match Periods</th>
            <th>Since</th>
            <th>To</th>
            <th>Category</th>
            <th>Score calculation</th>
            <th>Status</th>
        </tr>
        <?php foreach ($competitions as $competition): extract($competition->toArray()); ?>
        <tr>
            <td><strong><?php echo $name; ?></strong></td>
            <td><?php echo $type; ?></td>
            <td><?php echo $match_periods; ?></td>
            <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date_start, '-', 'd.m.Y'); ?></td>
            <td><?php echo \STM\Utils\Dates::convertDatetimeToString($date_end, '-', 'd.m.Y'); ?></td>
            <td><?php echo $category; ?></td>
            <td><?php echo $score_type; ?></td>
            <td><?php echo $status; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>Nebyli načteny žádné soutěže</p>
    <p>No competitions</p>
<?php endif; ?>