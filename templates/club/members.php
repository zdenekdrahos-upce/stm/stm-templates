
<h3>Club members/Členové týmu</h3>
<?php if (empty($clubMembers)): ?>
    <p>No members/Žádní členové</p>
<?php else: ?>
    <table>
        <thead>
            <tr>        
                <th>Member/Člen</th>
                <th>Position/pozice</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($clubMembers as $clubMember): extract($clubMember->toArray()); ?>
            <tr>
                <td><strong><?php echo $person; ?></strong></td>
                <td><?php echo $position; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
