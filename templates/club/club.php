<?php 
$clubDetails = $club->toArray();
?>

<h3>Club info/Informace o klubu</h3>
<ul>
    <li>Name/Jméno: <strong><?php echo $club; ?></strong></li>
    <li>City/Město: <strong><?php echo $clubDetails['city']; ?></strong></li>
    <li>Foundation Date/Datum založení: <strong><?php echo \STM\Utils\Dates::convertDatetimeToString($clubDetails['foundation_date'], '', 'd.m.Y'); ?></strong></li>
    <li>Club Info/Informace o klubu:    
        <aside class="text"><strong><?php echo $clubDetails['club_info']; ?></strong></aside>
    </li>
    <li>Colors/Barvy: <strong><?php echo $clubDetails['club_colors']; ?></strong></li>
    <li>Web: <strong><?php echo $clubDetails['website']; ?></strong></li>
    <li>Address/Adresa: <strong><?php echo $clubDetails['address']; ?></strong></li>
    <li>Email: <strong><?php echo $clubDetails['email_contact']; ?></strong></li>
    <li>Telephone/Telefon: <strong><?php echo $clubDetails['telephone_contact']; ?></strong></li>
    <li>Stadium/Stadion: <strong><?php echo $clubDetails['stadium']; ?></strong></li>
    <li>Country/Země: <strong><?php echo $clubDetails['country']; ?></strong></li>
</ul>