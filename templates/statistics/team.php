<h3>Overall stats/Celkové statistiky</h3>
<table>
    <tr>
        <th>&nbsp;</th>
        <th>Overall/Celkem</th>
        <th>Home/Doma</th>
        <th>Away/Venku</th>
        <th>Difference/Rozdíl</th>
    </tr>
    <?php foreach($statistics->tableRowStatistics as $key => $stat): ?>
    <tr>
        <th><?php echo $key; ?></th>
        <td><strong><?php echo $stat->getTotal(); ?></strong></td>
        <?php if ($key == 'truthPoints'): ?>
        <td><?php echo "{$stat->home}"; ?></td>
        <td><?php echo "{$stat->away}"; ?></td>
        <td>&nbsp;</td>
        <?php else: ?>
        <td><?php echo "{$stat->home} ({$stat->getPercentageHome()}%)"; ?></td>
        <td><?php echo "{$stat->away} ({$stat->getPercentageAway()}%)"; ?></td>
        <td><?php echo $stat->getDifference(); ?></td>
        <?php endif; ?>
    </tr>
    <?php endforeach; ?>
</table>

<h4>Records/Rekordy</h4>
<?php
$records = array('highestAggregateScore', 'highestGoalFor', 'highestGoalAgainst', 'highestWin', 'highestLoss');
foreach ($records as $record):
    $statistic = $statistics->$record;
?>
<h5><?php echo $record; ?></h5>
<ul>
    <li>
        Doma/Home: 
        <?php if (count($statistic->matchesHome) > 0): ?>
        <strong><?php echo "{$statistic->topValues->home}"; ?></strong> 
        (<?php 
        foreach($statistic->matchesHome as $id_match) {
            $match = $statistics->matches[$id_match];
            echo "{$match->match} {$match->score}";
            if ($id_match != end($statistic->matchesHome)) {
                echo ', ';
            }
        }
        ?>)
        <?php else: ?>
        <strong>-</strong>
        <?php endif; ?>
    </li>
    <li>
        Venku/away: 
        <?php if (count($statistic->matchesAway) > 0): ?>
        <strong><?php echo "{$statistic->topValues->away}"; ?></strong> 
        (<?php 
        foreach($statistic->matchesAway as $id_match) {
            $match = $statistics->matches[$id_match];
            echo "{$match->match} {$match->score}";
            if ($id_match != end($statistic->matchesAway)) {
                echo ', ';
            }
        }
        ?>)
        <?php else: ?>
        <strong>-</strong>
        <?php endif; ?>
    </li>
</ul>
<?php endforeach; ?>
