<h3>Referee in matches/Rozhodčí v zápasech</h3>
<?php if (empty($refereeInMatches)): ?>
    <p>No referees/Žádní rozhodčí</p>
<?php else: ?>
    <table>
        <thead>
            <tr>        
                <th>Match/Zápas</th>
                <th>Position/pozice</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($refereeInMatches as $referee): extract($referee->toArray()); ?>
            <tr>
                <td><strong><?php echo $match; ?></strong></td>
                <td><?php echo $position; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>    
<?php endif; ?>