<h3>Matches with statistic/Zápasy se zápisem do statistik</h3>
<?php if ($matchPlayerActions): ?>
    <table>
        <tr>
            <th>Match/Zápas</th>
            <th>Team/Tým</th>
            <th>Action/Akce</th>
            <th>When Action Happened/Kdy se akce odehrála</th>
            <th>Comment/Komentáře</th>
        </tr>
        <?php foreach ($matchPlayerActions as $match_player_action): extract($match_player_action->toArray()); ?>
        <tr>
            <td><?php echo $name_match; ?></td>
            <td><?php echo $name_team; ?></td>
            <td><?php echo $name_action; ?></td>
            <td><?php echo $minute_action; ?></td>
            <td><?php echo $action_comment; ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php else: ?>
    <p>No matches/Žádné zápasy</p>
<?php endif; ?>