
<h3>Person's teams/Člen týmu</h3>
<?php if (empty($memberOfTeams)): ?>
    <p>No teams/Žádné týmy</p>
<?php else: ?>
    <table>
        <thead>
            <tr>        
                <th>Team/Tým</th>
                <th>Position/Pozice</th>
                <th>Since/Od</th>
                <th>To/Do</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($memberOfTeams as $team_member): $team_member = $team_member->toArray(); ?>
            <tr>
                <td><strong><?php echo $team_member['team']; ?></strong></td>
                <td><?php echo $team_member['position']; ?></td>        
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($team_member['date_since'], '-', 'd.m.Y'); ?></td>
                <td><?php echo \STM\Utils\Dates::convertDatetimeToString($team_member['date_to'], '-', 'd.m.Y'); ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>