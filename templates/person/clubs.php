<h3>Person's clubs/Člen klubu</h3>
<?php if (empty($memberOfClubs)): ?>
    <p>No clubs/Žádné kluby</p>
<?php else: ?>
    <table>
        <thead>
            <tr>        
                <th>Club/Klub</th>
                <th>Position/pozice</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($memberOfClubs as $club_member): $club_member = $club_member->toArray(); ?>
            <tr>
                <td><strong><?php echo $club_member['club']; ?></strong></td>
                <td><?php echo $club_member['position']; ?></td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>    
<?php endif; ?>