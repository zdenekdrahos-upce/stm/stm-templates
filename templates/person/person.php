<h3>Person info/Informace o osobě</h3>
<ul>
    <li>Name/jméno: <strong><?php echo $person->__toString(); ?></strong></li>
    <li>Birth date/Datum narození: <strong><?php echo $birthDate; ?></strong></li>
    <li>Characteristic/Charakteristika: <strong><?php echo $personDetails['characteristic']; ?></strong></li>
    <li>Country/Země: <strong><?php echo $personDetails['country']; ?></strong></li>
</ul>
