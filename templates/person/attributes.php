<h3>Person's attributes/Osobní atributy</h3>
<?php if (empty($personAttributes)): ?>
    <p>No attributes/žádné atributy</p>
<?php else: ?>
    <table>
        <tr>
            <th>Attribute/Atribut</th>
            <th>Value/Hodnota</th>
        </tr>
        <?php foreach($personAttributes as $attribute): ?>
        <tr>
            <td><?php echo $attribute->getAttribute(); ?></td>
            <td><?php echo $attribute->getValue(); ?></td>
        </tr>
        <?php endforeach; ?>
    </table>
<?php endif; ?>